## Interface: 80000
## Name: ClassStatPriority
## Title: |cff4488ffClassStatPriority|r
## Notes: |cff666666By Josh "Kkthnx" Russell|r |n|n|cffffffffDisplays class and specialisation optimal stat priority by icy-veins.com |nAddOn for World of Warcraft.|r|r|n|n|cff4488ffThis addon supports:|r Battle for Azeroth |cff666666Patch: 8.0.1|r|n|n
## Author: Kkthnx
## Version: 1.0
## X-Credits:

Locales.lua
Core.lua