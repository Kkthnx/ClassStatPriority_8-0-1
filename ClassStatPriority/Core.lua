local addonName, vars = ...
local L = vars.L
local stats_Table = {}

local _G = _G
local select = select
local string_gsub = string.gsub

local CreateFrame = _G.CreateFrame
local GetSpecialization = _G.GetSpecialization
local GetSpecializationInfo = _G.GetSpecializationInfo
local IsAddOnLoaded = _G.IsAddOnLoaded
local UIParent = _G.UIParent

local sFrameInit = false
local pHooked = false
local elapsedTime = 0

-- Deathknight Blood
stats_Table[250] = "Str > Vers > Haste > Mast > Crit"
-- Deathknight Frost
stats_Table[251] = "Str > Mast > Crit > Vers > Haste"
-- Deathknight Unholy
stats_Table[252] = "Str > Haste > (Crit / Vers) > Mast"

-- Druid Balance
stats_Table[102] = "Int > Haste > Crit > Vers > Mast"
-- Druid Feral
stats_Table[103] = "Agi > Haste > Crit > Vers > Mast"
-- Druid Guardian
stats_Table[104] = "|c00ffff00Survival:|r (Armor/Agi/Stam)>Mast>Vers>Haste>Crit \n DPS Boost: Agi > Haste >= Crit >= Vers >= Mast"
-- Druid Restoration
stats_Table[105] = "Raid: Int > (Haste = Crit = Vers) > Mast \n |c00ffff00Dungeon:|r (Mast = Haste) Int > Vers > Crit"

-- Hunter Beastmaster
stats_Table[253] = "|c00ffff00Single Target:|r Agi > Haste > Crit > Mast > Vers \n |c00ffff00Multi Target:|r Agi > Mast > Haste > Crit > Vers"
-- Hunter Marksmanship
stats_Table[254] = "|c00ffff00Single Target:|r Agi > Haste > Mast > Vers > Crit \n |c00ffff00Multi Target:|r Agi > Mast > Haste > Crit > Vers"
-- Hunter Survival
stats_Table[255] = "|c00ffff00Single Target:|r Agi > Haste > Crit > Vers > Mast \n |c00ffff00Multi Target:|r Agi > Haste > Crit > Vers > Mast"

-- Mage Arcane
stats_Table[62] = "Int > Crit > Haste > Mast > Vers"
-- Mage Fire
stats_Table[63] = "Int > Mast > Vers > Haste > Crit"
-- Mage Frost
stats_Table[64] = "Int > Crit 33.34% > Haste > Vers > Mast > Crit"

-- Monk Brewmaster
stats_Table[268] = "Agi > (Crit = Vers = Mast) > Haste"
-- Monk Mistweaver
stats_Table[270] = "Raid: Int > Crit > Vers > Haste > Mast \n |c00ffff00Dungeon Mythic+:|r Int > (Haste = Mast) > Vers > Crit"
-- Monk Windwalker
stats_Table[269] = "Agi > Vers > Mast > Crit > Haste"

-- Paladin Holy
stats_Table[65] = "Int > Crit > Mast > Haste > Vers"
-- Paladin Protection
stats_Table[66] = "Haste > Mast > Vers > Crit"
-- Paladin Retribution
stats_Table[70] = "Str > Haste > (Crit ~= Vers ~= Mast)"

-- Priest Discipline
stats_Table[256] = "Int > Haste > Crit > Mast > Vers"
-- Priest Holy
stats_Table[257] = "|c00ffff00Raid:|r Int > (Mast = Crit) > Vers > Haste \n |c00ffff00Dungeon:|r Int > Crit > Haste > Vers > Mast"
-- Priest Shadow
stats_Table[258] = "Int > (Haste = Crit) > Mast > Vers"

-- Rogue Assassination
stats_Table[259] = "Agi > Haste > Crit > Mast > Vers"
-- Rogue Outlaw
stats_Table[260] = "Agi > Haste > Vers > Crit > Mast"
-- Rogue Subtlety
stats_Table[261] = "|c00ffff00Single Target:|r Agi > Crit > Haste > Mast > Vers \n |c00ffff00Multi Target:|r Agi > Mast > Crit > Vers > Haste"

-- Shaman Elemental
stats_Table[262] = "Int > Haste > Vers > Crit > Mast"
-- Shaman Enhancement
stats_Table[263] = "Agi > Haste > (Crit = Vers) > Mast"
-- Shaman Restoration
stats_Table[264] = "Int > Crit > Vers > (Haste = Mast)"

-- Warlock Affliction
stats_Table[265] = "Mast > Int > Haste > (Crit = Vers)"
-- Warlock Demonology
stats_Table[266] = "Int > Haste > Crit > Vers > Mast"
-- Warlock Destruction
stats_Table[267] = "Int > Haste > (Crit = Mast) > Vers"

-- Warrior Arms
stats_Table[71] = "Str > Haste > Crit > Mast > Vers"
-- Warrior Fury
stats_Table[72] = "Str > Haste > Mast > Vers > Crit"
-- Warrior Protection
stats_Table[73] = "Haste > Armor > Vers > Mast > Crit > Str"

-- Demon Hunter Havoc
stats_Table[577] = "Agi > (Haste = Vers) > Crit > Mast"
-- Demon Hunter Vengeance
stats_Table[581] = "Agi > Haste > Vers > Mast > Crit"

local stats_Frame = CreateFrame("Frame", "stats_Frame", UIParent)
local stats_Window = CreateFrame("Frame", "stats_Window", stats_Frame)
function stats_Frame:CreateWin()
	if PaperDollFrame:IsVisible() then
		if not stats_Frame.stats_txt then
			stats_Frame:SetBackdrop({
				bgFile = "Interface\\BUTTONS\\WHITE8X8",
				edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
				tile = false,
				tileEdge = false,
				tileSize = 12,
				edgeSize = 12,
				insets = {left = 3, right = 3, top = 3, bottom = 3}
			})
			stats_Frame:SetBackdropBorderColor(0.7, 0.7, 0.7)
			stats_Frame:SetBackdropColor(.04, .04, .04, 0.9)
			stats_Frame:SetFrameStrata("TOOLTIP")
			stats_Frame:SetWidth(PaperDollFrame:GetWidth() - 50)
			stats_Frame:SetHeight(45)

			stats_Frame.stats_txt = stats_Frame:CreateFontString(nil, "OVERLAY", "GameFontWhite")
			stats_Frame.stats_txt:ClearAllPoints()
			stats_Frame.stats_txt:SetAllPoints(stats_Frame)
			stats_Frame.stats_txt:SetJustifyH("CENTER")
			stats_Frame.stats_txt:SetJustifyV("CENTER")

			stats_Frame:ClearAllPoints()
			stats_Frame:SetPoint("BOTTOMRIGHT", PaperDollFrame, "TOPRIGHT", 0, 0)
			stats_Frame:SetParent(PaperDollFrame)
			stats_Frame:Show()
		end
		return true
	end
	return false
end

function stats_Frame:Update()
	if GetSpecialization() == nil then
		return false
	end

	local specID = select(1, GetSpecializationInfo(GetSpecialization()))
	if stats_Frame:CreateWin() then
		local s = stats_Table[specID]

		if s then
			-- H.sch End For Multiple Language
			s = string_gsub(s, "Strength", "Str")
			s = string_gsub(s, "Agility", "Agi")
			s = string_gsub(s, "Intellect", "Int")
			s = string_gsub(s, "Stamina", "Stam")
			s = string_gsub(s, "Versatility", "Vers")

			s = string_gsub(s, "Int", L["Int"])
			s = string_gsub(s, "Crit", L["Crit"])
			s = string_gsub(s, "Str", L["Str"])
			s = string_gsub(s, "Agi", L["Agi"])
			s = string_gsub(s, "Stam", L["Sta"])
			s = string_gsub(s, "Vers", L["Vers"])
			s = string_gsub(s, "Haste", L["Haste"])
			s = string_gsub(s, "Mast", L["Mastery"])
			s = string_gsub(s, "Armor", L["Armor"])

			stats_Frame.stats_txt:SetText(s)
		end
		sFrameInit = true
	end
end

stats_Frame:RegisterEvent("SPELLS_CHANGED")
stats_Frame:RegisterEvent("ADDON_LOADED")
stats_Frame:SetScript("OnEvent", function(self, event, ...)
	if event == "ADDON_LOADED" and ... == addonName then
		stats_Frame:Update()
		PaperDollFrame:HookScript("OnShow", function()
			stats_Frame:Update()
		end)
		pHooked = true
	elseif event == "SPELLS_CHANGED" then
		if IsAddOnLoaded(addonName) then
			stats_Frame:Update()
		end
	end
end)

local delayTimer = CreateFrame("Frame")
delayTimer:SetScript("OnUpdate", function (self, elapsed)
	elapsedTime = elapsedTime + elapsed

	if (elapsedTime < 10) then
		return
	else
		elapsedTime = 0
	end

	if not sFrameInit then
		stats_Frame:Update()
		if not pHooked then
			PaperDollFrame:HookScript("OnShow", function()
				stats_Frame:Update()
			end)
			pHooked = true
		end
	end
end)